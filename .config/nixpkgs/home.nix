{ config, pkgs, ... }:

let
  unstable = import <unstable> {};
in {
  home.packages = [
    pkgs.bashInteractive
    unstable.nodejs-12_x
  ];
  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;
}
