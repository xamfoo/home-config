#!/usr/bin/env bash

set -eo pipefail
DIR="$(cd $(dirname "${BASH_SOURCE[0]}") >/dev/null 2>&1 && pwd)"
source $DIR/config.sh
set -u

ensure_nix_installed() {
  if ! command -v nix >/dev/null 2>&1; then
    rm -rf $HOME/.nix-*
    [ -d /nix ] && rm -rf /nix || sudo rm -rf /nix
    $DIR/install-nix-$nix_version --no-daemon
  fi
}

configure_nix_channel() {
  nix-channel --add https://github.com/rycee/home-manager/archive/$nix_home_manager_branch.tar.gz home-manager
  nix-channel --add https://nixos.org/channels/$nix_pkgs_channel nixpkgs
  nix-channel --add https://nixos.org/channels/nixpkgs-unstable unstable
}

ensure_nix_home_manager_installed() {
  if ! command -v home-manager >/dev/null 2>&1; then
    mkdir -m 0755 -pv /nix/var/nix/{profiles,gcroots}/per-user/$USER
    nix-channel --update
    nix-shell '<home-manager>' -A install
  fi
}

ensure_nix_installed
configure_nix_channel
ensure_nix_home_manager_installed
home-manager switch
