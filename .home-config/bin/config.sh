nix_version="2.3"
nix_home_manager_branch="release-19.03"
if [ "$OSTYPE" == "darwin"* ]; then
  nix_pkgs_channel="nixpkgs-19.03-darwin"
else
  nix_pkgs_channel="nixos-19.03"
fi
